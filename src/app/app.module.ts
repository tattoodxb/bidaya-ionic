import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { HttpClientModule, HttpClient } from "@angular/common/http";
import { HttpModule,  } from "@angular/http";
import { CalculatorProvider } from '../providers/calculator/calculator';
import { AppServiceApiProvider } from '../providers/app-service-api/app-service-api';
import { TranslateModule, TranslateLoader  } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { FinanceCalcPage } from '../pages/finance-calc/finance-calc';
import { FinanceCalcResultPage } from '../pages/finance-calc-result/finance-calc-result';
import { InstallmentCalcPage } from '../pages/installment-calc/installment-calc';
import { InstallmentCalcResultPage } from '../pages/installment-calc-result/installment-calc-result';
import { BuyRentCalcPage } from '../pages/buy-rent-calc/buy-rent-calc';
import { BuyRentCalcResultPage } from '../pages/buy-rent-calc-result/buy-rent-calc-result';
import { ExpenseCalcPage } from '../pages/expense-calc/expense-calc';
import { ExpenseCalcResultPage } from '../pages/expense-calc-result/expense-calc-result';
import { RedfCalcPage } from '../pages/redf-calc/redf-calc';
import { RedfCalcResultPage } from '../pages/redf-calc-result/redf-calc-result';
import { PayReturnCalcPage } from '../pages/pay-return-calc/pay-return-calc';
import { PayReturnCalcResultPage } from '../pages/pay-return-calc-result/pay-return-calc-result';
import "rxjs/add/operator/map";

export function setTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    FinanceCalcPage,
    FinanceCalcResultPage,
    InstallmentCalcPage,
    BuyRentCalcPage,
    BuyRentCalcResultPage,
    InstallmentCalcResultPage,
    ExpenseCalcPage,
    RedfCalcPage,
    ExpenseCalcResultPage,
    PayReturnCalcPage,
    PayReturnCalcResultPage,
    RedfCalcResultPage
  ],
  imports: [
    HttpModule,
    HttpClientModule,
    BrowserModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (setTranslateLoader),
        deps: [HttpClient]
      }
    }),
    IonicModule.forRoot(MyApp, {mode: 'ios'}),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    FinanceCalcPage,
    FinanceCalcResultPage,
    InstallmentCalcPage,
    BuyRentCalcPage,
    BuyRentCalcResultPage,
    InstallmentCalcResultPage,
    ExpenseCalcPage,
    RedfCalcPage,
    ExpenseCalcResultPage,
    PayReturnCalcPage,
    PayReturnCalcResultPage,
    RedfCalcResultPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    GoogleAnalytics,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    CalculatorProvider,
    InAppBrowser,
    AppServiceApiProvider
  ]
})

export class AppModule {

}


