import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, MenuController, NavController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
//import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { HomePage } from '../pages/home/home';
import { TranslateService } from '@ngx-translate/core';
import * as $ from "../assets/js/jquery-1.12.3.min.js";
import "rxjs/add/operator/map";
//declare var dataLayer: Array<String>;

@Component({
  templateUrl: 'app.html'
})


export class MyApp {
  @ViewChild(Nav) nav: NavController;

  rootPage: any = HomePage;

  public homePage   = {title: "Home", component : HomePage};
  public langSwitch = "ar";;
  public menuSide   = "right";
  regions : any;

  pages: Array<{title: string, component: any}>;

  options : InAppBrowserOptions = {
    location : 'yes',//Or 'no' 
    hidden : 'no', //Or  'yes'
    clearcache : 'yes',
    clearsessioncache : 'yes',
    zoom : 'yes',//Android only ,shows browser zoom controls 
    hardwareback : 'yes',
    mediaPlaybackRequiresUserAction : 'no',
    shouldPauseOnSuspend : 'no', //Android only 
    closebuttoncaption : 'Close', //iOS only
    disallowoverscroll : 'no', //iOS only 
    toolbar : 'yes', //iOS only 
    enableViewportScale : 'no', //iOS only 
    allowInlineMediaPlayback : 'no',//iOS only 
    presentationstyle : 'pagesheet',//iOS only 
    fullscreen : 'yes',//Windows only    
  };

  constructor(private iab: InAppBrowser,
              //private ga: GoogleAnalytics,
              public platform: Platform, 
              public statusBar: StatusBar, 
              public splashScreen: SplashScreen,
              public translate: TranslateService,
              public menuCtrl:MenuController) {
    
    this.initializeApp();
    this.initTranslate();
  }

  initTranslate()
  {
     // Set the default language for translation strings, and the current language.
     this.translate.setDefaultLang('en');
     this.platform.setDir('ltr', true);

     if (this.translate.getBrowserLang() !== undefined)
     {
         this.translate.use(this.translate.getBrowserLang());
     }
     else
     {
         this.translate.use('en'); // Set your language here
     }
  }

  switchLang() {

    this.translate.use(this.langSwitch);
    if(this.langSwitch == "ar")
    {
        this.langSwitch = "en";
        this.platform.setDir('rtl', true);  
        $("body").addClass("ar").removeClass("en");
    }
    else 
    {
       this.langSwitch = "ar";
       this.platform.setDir('ltr', true);
       $("body").addClass("en").removeClass("ar");
    }
    this.openPage(this.homePage);

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      // dataLayer.push({
      //   'event' : 'Home',
      //   'category' : 'Home',
      //   'action' : 'Initialize'
      // });
    });
    
  }

  openPage(page) {
    this.nav.setRoot(page.component);    
  }

  openBrowser(url : string){
    let target = "_system";
    this.iab.create(url,target,this.options);
  }
  
}
