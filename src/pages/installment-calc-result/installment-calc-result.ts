import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ToastController, LoadingController, Platform } from 'ionic-angular';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { HttpClient,HttpParams } from '@angular/common/http';
import { AppServiceApiProvider } from '../../providers/app-service-api/app-service-api';
import { AlertInputValidator } from  '../../validators/alertinput';
declare var dataLayer: Array<any>;
/**
 * Generated class for the InstallmentCalcResultPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-installment-calc-result',
  templateUrl: 'installment-calc-result.html',
})
export class InstallmentCalcResultPage {

  public result = [];

  public fullnameLabel = "Fullname";
  public emailLabel = "Email";
  public mobileLabel = "Mobile";
  public cancelButton = "Cancel";
  public sendButton = "Send";
  public emailSentSuccess = "Email Sent";
  public emailSentError = "Unable to send email, please try again.";
  public pleaseWaitText = "Please wait...";
  public emailResultLabel = "Email Result";

  options : InAppBrowserOptions = {
    location : 'yes',//Or 'no' 
    hidden : 'no', //Or  'yes'
    clearcache : 'yes',
    clearsessioncache : 'yes',
    zoom : 'yes',//Android only ,shows browser zoom controls 
    hardwareback : 'yes',
    mediaPlaybackRequiresUserAction : 'no',
    shouldPauseOnSuspend : 'no', //Android only 
    closebuttoncaption : 'Close', //iOS only
    disallowoverscroll : 'no', //iOS only 
    toolbar : 'yes', //iOS only 
    enableViewportScale : 'no', //iOS only 
    allowInlineMediaPlayback : 'no',//iOS only 
    presentationstyle : 'pagesheet',//iOS only 
    fullscreen : 'yes',//Windows only    
  };

  constructor(public navCtrl: NavController, 
    private http: HttpClient, 
    public navParams: NavParams,
    private iab: InAppBrowser,
    public alertCtrl: AlertController,
    public toastCtrl : ToastController,
    public loadingCtrl : LoadingController,
    private device: Platform,
    private appServiceProvider : AppServiceApiProvider) {

    this.result = navParams.get('data');
    // GET TRANSLATION FOR SYSTEM ALERT WINDOW
    let alertLang = this.appServiceProvider.getAlertTranslation();
    this.fullnameLabel = alertLang['fullnameLabel'];
    this.emailLabel = alertLang['emailLabel'];
    this.mobileLabel = alertLang['mobileLabel'];
    this.cancelButton = alertLang['cancelButton'];
    this.sendButton = alertLang['sendButton'];
    this.emailSentSuccess = alertLang['emailSentSuccess'];
    this.emailSentError = alertLang['emailSentError'];
    this.pleaseWaitText = alertLang['pleaseWaitText'];
    this.emailResultLabel = alertLang['emailResultLabel'];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InstallmentCalcResultPage');
    let platform = "Android";
    if (this.device.is('ios')) {
      platform = "Apple";
    }
    dataLayer.push({
      'event' : ' Calculator Usage',
      'category' : 'Calculator Usage',
      'action' : 'Monthly Installment',
      'label' : platform + ' app'
    });
  }

  openBrowser(url : string){
    let target = "_system";
    let host = "https://www.bidaya.com.sa/"+this.appServiceProvider.getCurrentLang()+"/";
    this.iab.create(host + url,target,this.options);
  }

  openSendResult() {

    let win = this.alertCtrl.create({
      title : this.emailResultLabel,
      inputs: [
        {
          type : "text",
          name : "fullname",
          placeholder: this.fullnameLabel
        },
        {
          type: "email",
          name : "email",
          placeholder: this.emailLabel
        },
        {
          type: "tel",
          name: "mobile",
          placeholder: this.mobileLabel
        }
      ],
      buttons : [
        {
          text: this.cancelButton
        },
        {
          text : this.sendButton,
          handler: data => {
            
            // VALIDATE SEND RESULT FORM
            let err_msg = this.checkFormErrors(data);
            if(err_msg != "")
            {
                this.showToast(err_msg);
                return false;
            }

            // DISMISS IF FORM IS VALID
            win.onDidDismiss(() => {

                // EMAIL RESULT
                this.emailResult(data);

            });

          } // end handler
        }
      ],
      enableBackdropDismiss: false
    });

    win.present();
  }

  showToast(message : string) {

      let toast = this.toastCtrl.create({
        message: message,
        duration: 3000,
        position: 'top'
      });

      toast.present();
  }

  emailResult(data) {

      // SHOW LOADING 
      let loading = this.loadingCtrl.create({
        content: this.pleaseWaitText
      });
  
      loading.present();

      if(data.email != "")
      {
          this.result['fullname'] = data.fullname;
          this.result['email'] = data.email;
          this.result['number'] = data.mobile;
      
          let postdata = this.appServiceProvider.generateFormData(this.result);
      
          let url = this.appServiceProvider.emailResultUrl;
          let headerOptions = { headers: {'Content-Type': 'application/x-www-form-urlencoded'} };
          let params = new HttpParams();
              params = params.append('lang', this.appServiceProvider.getCurrentLang());
              params = params.append('uname', this.appServiceProvider.getUsername());
              params = params.append('secret', this.appServiceProvider.getSecret());
            
          // LOOP TO ADD THE PARAMS
          Object.keys(postdata).forEach( key => {    
            params =  params.append(key, postdata[key]);
          });
              
            
          this.http.post( url, params, headerOptions )
            .subscribe((response) => {
              
              // DISMISS LOADING
              loading.dismiss();
      
              if(response['status'] == 400)
              {
                this.showToast(this.emailSentSuccess);
              }
              else
              {
                this.showToast(this.emailSentError);
              }
      
          }); // end http
      }
      else
      {
          // DISMISS LOADING
          loading.dismiss();
          this.showToast(this.emailSentError);
          return false;
      }

  }

  checkFormErrors(data :  any) {

      let errors = [];
      let err_msg = "";
      let namecheck = AlertInputValidator.fullnameIsValid(data.fullname);
      let emailcheck = AlertInputValidator.emailIsValid(data.email); 
      let mobilecheck = AlertInputValidator.mobileIsValid(data.mobile);

      if(namecheck !=  null)
      {
        errors['namecheck'] = namecheck;
      }

      if(emailcheck !=  null)
      {
        errors['emailcheck'] = emailcheck;
      }

      if(mobilecheck !=  null)
      {
        errors['mobilecheck'] = mobilecheck;
      }
    
      if(Object.keys(errors).length > 0)
      {
          // LOOP THROUGH EACH ERRORS
          Object.keys(errors).forEach( key => {  
            Object.keys(errors[key]).forEach( error_key => {    
              if(error_key == this.appServiceProvider.getCurrentLang()) {
                 err_msg += errors[key][error_key] + "\n";
              }
            });
        });

      }

      return err_msg;

  }

}
