import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
//import { HttpClient } from '@angular/common/http';
import { Validators, FormBuilder, FormGroup } from "@angular/forms";
import { BuyRentCalcResultPage } from '../buy-rent-calc-result/buy-rent-calc-result';
import { CalculatorProvider }  from '../../providers/calculator/calculator';
import { AppServiceApiProvider } from '../../providers/app-service-api/app-service-api';
//import { GoogleAnalytics } from '@ionic-native/google-analytics';
declare var dataLayer: Array<any>;

/**
 * Generated class for the BuyRentCalcPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-buy-rent-calc',
  templateUrl: 'buy-rent-calc.html',
})
export class BuyRentCalcPage {

  private calcForm : FormGroup;
  public buyPropertyPriceFormatted = "150,000";
  public rentPropertyPriceFormatted = "500";

  sectors : any;

  constructor(public navCtrl: NavController,
              //private ga: GoogleAnalytics,
              public navParams: NavParams,
              private formBuilder : FormBuilder,
              //private http: HttpClient, 
              private calculatorProvider : CalculatorProvider,
              private appServiceProvider : AppServiceApiProvider,
              private toastCtrl : ToastController) {

     // FORM VALIDATION
     this.calcForm = this.formBuilder.group({
      buyPropertyPrice: [150000, Validators.required],
      rentPropertyPrice: [500, Validators.required],
      downPayment: [15, Validators.required],
      repayment_terms: [1, Validators.required],
      sector: ['GOVT', Validators.required],
      calcMode: ['6'],
     });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BuyRentCalcPage');
    dataLayer.push({
      'event' : 'appScreenView',
      'screenPath' : 'Buy & Rent',
      'screenName' : 'Buy & Rent',
    });

    /*this.ga.startTrackerWithId(this.appServiceProvider.gtmTag).then(() => {      
      this.ga.trackEvent('Calculator Usage', 'Buy & Rent', 'Calculator Usage', 0, true);
    }).catch(e => console.log('Error starting GoogleAnalytics', e));*/
    
    // GET SECTORS
    this.appServiceProvider.getAppData()
        .subscribe(response => {
          this.sectors = response['select_sectors'];
      },
      error => {
        console.log(error);
    });    
  }

  onSlideChange(input, fieldname) {
   
    switch(fieldname) {

      case 'buyPropertyPrice': 
            this.buyPropertyPriceFormatted = this.calculatorProvider.numberWithCommas(input);
            break;
      case 'rentPropertyPrice': 
            this.rentPropertyPriceFormatted = this.calculatorProvider.numberWithCommas(input);
            break;
    }
  }

  submitForm() {

    // CHECK IF FORM IS VALID
    if(this.calcForm.valid) {
      document.getElementById("btnsubmit").setAttribute("disabled", "disabled");
      let formData = this.calcForm.value;
      // CALCULATE RESULT BASED ON DATA PASSED
      let calcResult = this.calculatorProvider.calculate(formData);
      // SHOW FINNANCIAL CALCULATOR RESULT PAGE
      this.navCtrl.push(BuyRentCalcResultPage,  {data : calcResult});
      document.getElementById("btnsubmit").removeAttribute("disabled");
    }
    else {
      // SHOW ERROR MESSAGE
      this.showToast('Invalid form. Please check your inputs.', 'middle');
      return false;
    }

  }

  showToast(message : string, position : string) {

      let toast = this.toastCtrl.create({
        message: message,
        duration: 3000,
        position: position
      });

      toast.present();
  }

}
