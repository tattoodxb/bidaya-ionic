import { Component, ViewChild } from '@angular/core';
import { Nav, NavController, Platform } from 'ionic-angular';
import { FinanceCalcPage} from "../finance-calc/finance-calc";
import { InstallmentCalcPage} from "../installment-calc/installment-calc";
import { BuyRentCalcPage} from "../buy-rent-calc/buy-rent-calc";
import { ExpenseCalcPage } from '../expense-calc/expense-calc';
import { RedfCalcPage } from '../redf-calc/redf-calc';
import { PayReturnCalcPage } from '../pay-return-calc/pay-return-calc';
import { HttpClient } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';
import * as $ from "../../assets/js/jquery-1.12.3.min.js";
import 'rxjs/Rx';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  @ViewChild(Nav) nav: NavController;
  rootPage: any = HomePage;
  public homePage   = {title: "Home", component : HomePage};
  public langSwitch = "ar";

  public financeCalcPage = FinanceCalcPage;
  public installmentCalcPage = InstallmentCalcPage;
  public buyRentCalcPage = BuyRentCalcPage;
  public expenseCalcPage = ExpenseCalcPage;
  public redfCalcPage = RedfCalcPage;
  public payReturnCalcPage = PayReturnCalcPage;  

  constructor(public navCtrl: NavController, public platform: Platform, public translate: TranslateService, public http: HttpClient) {
    this.initTranslate();
  }

  ionViewDidLoad() {
    //console.log("I'm alive!");
    //var h = window.innerHeight;
  }

  initTranslate()
  {
     // Set the default language for translation strings, and the current language.
     this.translate.setDefaultLang('en');
     this.platform.setDir('ltr', true);

     if (this.translate.getBrowserLang() !== undefined)
     {
         this.translate.use(this.translate.getBrowserLang());
     }
     else
     {
         this.translate.use('en'); // Set your language here
     }
  }

  switchLang() {

    this.translate.use(this.langSwitch);
    if(this.langSwitch == "ar")
    {
        this.langSwitch = "en";
        this.platform.setDir('rtl', true);  
        $("body").addClass("ar").removeClass("en");
    }
    else 
    {
       this.langSwitch = "ar";
       this.platform.setDir('ltr', true);
       $("body").addClass("en").removeClass("ar");
    }
  } 
}
