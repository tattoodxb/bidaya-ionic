import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Validators, FormBuilder, FormGroup } from "@angular/forms";
import { AppServiceApiProvider } from '../../providers/app-service-api/app-service-api';
import { CalculatorProvider }  from '../../providers/calculator/calculator';
import { InstallmentCalcResultPage } from '../installment-calc-result/installment-calc-result';
import { DownPaymentValidator } from  '../../validators/downpayment';
//import { GoogleAnalytics } from '@ionic-native/google-analytics';
import "rxjs/add/operator/map";
declare var dataLayer: Array<any>;

/**
 * Generated class for the InstallmentCalcPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-installment-calc',
  templateUrl: 'installment-calc.html',
})
export class InstallmentCalcPage {

  private calcForm : FormGroup;

  regions : any;
  sectors : any;
  public sectorRates = [];

  public propertyPriceFormatted = "";
  public propertyPrice = 0;
  public downPaymentFormatted = "";
  public downPayment = 0;

  public dpIsHigher = false;

  public companies = [];
  companyList : any;
  public company = "";

  constructor(private calculatorProvider : CalculatorProvider, 
              private http: HttpClient, 
              private formBuilder : FormBuilder, 
              public navCtrl: NavController, 
              public navParams: NavParams,
              private appServiceProvider : AppServiceApiProvider,
              private toastCtrl : ToastController) {

    // FORM VALIDATION
    this.calcForm = this.formBuilder.group({
      propertyPrice: ['',Validators.required],
      age: ['', Validators.compose([Validators.required, Validators.min(21), Validators.max(64)])],
      downPayment: ['', Validators.required],
      location: ['', Validators.required],
      firsthomebuyer: ['', Validators.required],
      sector: ['', Validators.required],
      repayment_terms: ['', Validators.compose([Validators.required, Validators.min(3), Validators.max(30)])],
      interesttype: ['', Validators.required],
      calcMode: ['2'],
      company: [''],
      propertyPriceFormatted: ['', Validators.required],
      downPaymentFormatted : ['', Validators.required],
    }, { 'validator': DownPaymentValidator.isValid });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InstallmentCalcPage');
    dataLayer.push({
      'event' : 'appScreenView',
      'screenPath' : 'Monthly Installment',
      'screenName' : 'Monthly Installment',
    });
   
    // GET REGIONS
    this.appServiceProvider.getAppData()
        .subscribe(response => {
          this.regions = response['regions'];
      },
      error => {
        console.log(error);
    });

     // GET SECTORS
    this.appServiceProvider.getAppData()
          .subscribe(response => {
            this.sectors = response['select_sectors'];
        },
        error => {
          console.log(error);
    }); 
    
  }

  submitForm() {

    // CHECK IF FORM IS VALID
    if(this.calcForm.valid) {
      document.getElementById("btnsubmit").setAttribute("disabled", "disabled");
      let formData = this.calcForm.value;
      let calcResult = [];
      calcResult['sector'] = this.calcForm.controls.sector.value; // ORIGINAL SECTOR

      if(this.calcForm.controls.company.value.trim() != '')
      {

            let url = this.appServiceProvider.checkCompanyUrl;
            let headerOptions = { headers: {'Content-Type': 'application/x-www-form-urlencoded'} };
            let params = new HttpParams();
                params = params.append('lang', this.appServiceProvider.getCurrentLang());
                params = params.append('uname', this.appServiceProvider.getUsername());
                params = params.append('secret', this.appServiceProvider.getSecret());
                params = params.append('companyname', this.calcForm.controls.company.value);

            // SUBMIT FORM
            this.http.post( url, params, headerOptions )
                .subscribe((response) => {

                  // PASS NEW SECTOR RECEIVE FROM THE RESPONSE
                  if(response['value']!='')
                  {
                    formData['sector'] = response['value'];
                  }
                  // CALCULATE RESULT BASED ON THE INPUTS 
                  calcResult = this.calculatorProvider.calculate(formData);
                  // PUSH TO RESULT PAGE
                  this.navCtrl.push(InstallmentCalcResultPage,  {data : calcResult});
              },
              error => {
                console.log(error);
            });

      }
      else
      {
         // CALCULATE RESULT BASED ON THE INPUTS 
         calcResult = this.calculatorProvider.calculate(formData);
         // PUSH TO RESULT PAGE
         this.navCtrl.push(InstallmentCalcResultPage,  {data : calcResult});
      }
      document.getElementById("btnsubmit").removeAttribute("disabled");
    }
    else {
      // SHOW ERROR MESSAGE
      this.showToast('Invalid form. Please check your inputs.', 'middle');
      return false;
    }
  }

  currencyFormat(event, key) {

    let amount = event.target.value;
    amount = amount.replace(/[^0-9.]/g, "");
    let amountIsValid = isNaN(parseFloat(amount));

    switch(key) {

      case 'propertyPriceFormatted':
            this.propertyPriceFormatted = this.calculatorProvider.numberWithCommas(amount);
            this.propertyPrice = (amountIsValid)? 0 : parseFloat(amount);
            break;
   
      case 'downPaymentFormatted':
            this.downPaymentFormatted = this.calculatorProvider.numberWithCommas(amount);
            this.downPayment = (amountIsValid)? 0 : parseFloat(amount);
            break;
    } // end switch

    // VALIDATE DOWNPAYMENT AND PROPERTY PRICE
    if(this.downPayment > this.propertyPrice) {
      this.dpIsHigher = true;
    }
    else {
      this.dpIsHigher =  false;
    }

  }

  autoCompleteCompany(event) {

    let keyword = event.target.value;
    // IF KEYWORD IS EMPTY CLEAR COMPANIES PROPERTY
    if(keyword.length == 0)
    {
       this.companies = [];
    }
    else
    {
      // CHECK IF COMPANY LIST IS EMPTY
      if(this.companyList == undefined) {
        
        // INITIALIZE COMPANYLIST
        let url = this.appServiceProvider.autofillCompanyUrl;
        let headerOptions = { headers: {'Content-Type': 'application/x-www-form-urlencoded'} };
        let params = new HttpParams();
            params = params.append('lang', this.appServiceProvider.getCurrentLang());
            params = params.append('uname', this.appServiceProvider.getUsername());
            params = params.append('secret', this.appServiceProvider.getSecret());
          
        this.http.post( url, params, headerOptions )
          .map(response => response)
          .subscribe((response) => {
              this.companyList = response;
              this.companies = this.companyList.filter(item => item.toLowerCase().startsWith(keyword.toLowerCase()));
        }); 
      }
      else {
        // FILTER COMPANY LIST BASED ON KEYWORD
        this.companies = this.companyList.filter(item => item.toLowerCase().startsWith(keyword.toLowerCase()));
      }    
    }

  }

  itemSelect(item) {
    this.company = item;
    // AFTER SELECT CLEAR COMPANIES DROPDOWN RESULT ARRAY
    this.companies = [];
  }

  showToast(message : string, position : string) {

    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: position
    });

    toast.present();
  }

}
