import { Component } from '@angular/core';
import { NavController, NavParams, ToastController} from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from "@angular/forms";
import { CalculatorProvider }  from '../../providers/calculator/calculator';
import { ExpenseCalcResultPage } from '../expense-calc-result/expense-calc-result';
//import { AppServiceApiProvider } from '../../providers/app-service-api/app-service-api';
//import { GoogleAnalytics } from '@ionic-native/google-analytics';
import "rxjs/add/operator/map";
declare var dataLayer: Array<any>;

/**
 * Generated class for the ExpenseCalcPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-expense-calc',
  templateUrl: 'expense-calc.html',
})
export class ExpenseCalcPage {

  // DECLARE AND INITIALIZE PROPERTIES
  private calcForm : FormGroup;
  public regions = [];
  public sectors = [];
  public sectorRates = [];

  public salaryFormatted = "";
  public salary = 0;
  public rentFormatted = "";
  public rent = 0;
  public maintenanceFormatted = "";
  public maintenance = 0;
  public foodFormatted = "";
  public food = 0;
  public communicationFormatted = "";
  public communication = 0;
  public utilityFormatted = "";
  public utility = 0;
  public healthcareFormatted = "";
  public healthcare = 0;
  public childcareFormatted = "";
  public childcare = 0;
  public carRentalFormatted = "";
  public carRental = 0;
  public otherFormatted = "";
  public other = 0;
  public tuitionFormatted = "";
  public tuition = 0;
  public restaurantFormatted = "";
  public restaurant = 0;
  public travelFormatted = "";
  public travel = 0;
  public apparelFormatted = "";
  public apparel = 0;
  public otherLeisureFormatted = "";
  public otherLeisure = 0;
  public creditCardFormatted = "";
  public creditCard = 0;
  public otherBillsFormatted = "";
  public otherBills = 0;
  public personalLoanFormatted = "";
  public personalLoan = 0;

  public companies = [];
  public companyList = [];
  public company = "";

  public totalExpense = 0;

  constructor(private calculatorProvider : CalculatorProvider, 
    private formBuilder : FormBuilder,
    public navCtrl: NavController, 
    public navParams: NavParams,
    //private appServiceProvider : AppServiceApiProvider,
    private toastCtrl : ToastController) {

    // FORM VALIDATION
    this.calcForm = this.formBuilder.group({
      calcMode : ['4'],
      totalExpense: [''],
      salary: ['', Validators.required],
      rent : [''],
      maintenance : [''],
      food: [''],
      communication: [''],
      utility: [''],
      healthcare: [''],
      childcare: [''],
      carRental: [''],
      other: [''],
      tuition: [''],
      restaurant: [''],
      travel: [''],
      apparel: [''],
      otherLeisure: [''],
      creditCard: [''],
      otherBills: [''],
      personalLoan: [''],
      salaryFormatted : ['', Validators.required],
      rentFormatted : [''],
      maintenanceFormatted : [''],
      foodFormatted : [''],
      communicationFormatted : [''],
      utilityFormatted : [''],
      healthcareFormatted : [''],
      childcareFormatted : [''],
      carRentalFormatted : [''],
      otherFormatted : [''],
      tuitionFormatted : [''],
      restaurantFormatted : [''],
      travelFormatted : [''],
      apparelFormatted : [''],
      otherLeisureFormatted : [''],
      creditCardFormatted : [''],
      otherBillsFormatted : [''],
      personalLoanFormatted : [''],

    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ExpenseCalcPage');
    dataLayer.push({
      'event' : 'appScreenView',
      'screenPath' : 'Expense Calculator',
      'screenName' : 'Expense Calculator',
    });
  }

  currencyFormat(event, key){

    let amount = event.target.value;
    amount = amount.replace(/[^0-9.]/g, "");
    let amountIsValid = isNaN(parseFloat(amount));
 
    switch(key) {
      
      case 'salaryFormatted':  
            this.salaryFormatted = this.calculatorProvider.numberWithCommas(amount);
            this.salary = (amountIsValid)? 0 : parseFloat(amount);
            break;
   
      case 'rentFormatted':          
            this.rentFormatted = this.calculatorProvider.numberWithCommas(amount);
            this.rent = (amountIsValid)? 0 : parseFloat(amount);
            break;

      case 'maintenanceFormatted':          
            this.maintenanceFormatted = this.calculatorProvider.numberWithCommas(amount);
            this.maintenance = (amountIsValid)? 0 : parseFloat(amount);
            break;

      case 'foodFormatted':           
            this.foodFormatted = this.calculatorProvider.numberWithCommas(amount);
            this.food = (amountIsValid)? 0 : parseFloat(amount);
            break;
   
      case 'communicationFormatted':          
            this.communicationFormatted = this.calculatorProvider.numberWithCommas(amount);
            this.communication = (amountIsValid)? 0 : parseFloat(amount);
            break;

      case 'utilityFormatted':           
            this.utilityFormatted = this.calculatorProvider.numberWithCommas(amount);
            this.utility = (amountIsValid)? 0 : parseFloat(amount);
            break;

      case 'healthcareFormatted':           
            this.healthcareFormatted = this.calculatorProvider.numberWithCommas(amount);
            this.healthcare = (amountIsValid)? 0 : parseFloat(amount);
            break;
   
      case 'childcareFormatted':         
            this.childcareFormatted = this.calculatorProvider.numberWithCommas(amount);
            this.childcare = (amountIsValid)? 0 : parseFloat(amount);
            break;

      case 'carRentalFormatted':            
            this.carRentalFormatted = this.calculatorProvider.numberWithCommas(amount);
            this.carRental = (amountIsValid)? 0 : parseFloat(amount);
            break;

      case 'otherFormatted':           
            this.otherFormatted = this.calculatorProvider.numberWithCommas(amount);
            this.other = (amountIsValid)? 0 : parseFloat(amount);
            break;
          
      case 'tuitionFormatted':          
            this.tuitionFormatted = this.calculatorProvider.numberWithCommas(amount);
            this.tuition = (amountIsValid)? 0 : parseFloat(amount);
            break;
   
      case 'restaurantFormatted':           
            this.restaurantFormatted = this.calculatorProvider.numberWithCommas(amount);
            this.restaurant = (amountIsValid)? 0 : parseFloat(amount);
            break;

      case 'travelFormatted':           
            this.travelFormatted = this.calculatorProvider.numberWithCommas(amount);
            this.travel = (amountIsValid)? 0 : parseFloat(amount);
            break;

      case 'apparelFormatted':           
            this.apparelFormatted = this.calculatorProvider.numberWithCommas(amount);
            this.apparel = (amountIsValid)? 0 : parseFloat(amount);
            break;

      case 'otherLeisureFormatted':           
            this.otherLeisureFormatted = this.calculatorProvider.numberWithCommas(amount);
            this.otherLeisure = (amountIsValid)? 0 : parseFloat(amount);
            break;
   
      case 'creditCardFormatted':           
            this.creditCardFormatted = this.calculatorProvider.numberWithCommas(amount);
            this.creditCard = (amountIsValid)? 0 : parseFloat(amount);
            break;

      case 'otherBillsFormatted':            
            this.otherBillsFormatted = this.calculatorProvider.numberWithCommas(amount);
            this.otherBills = (amountIsValid)? 0 : parseFloat(amount);
            break;

      case 'personalLoanFormatted':            
            this.personalLoanFormatted = this.calculatorProvider.numberWithCommas(amount);
            this.personalLoan = (amountIsValid)? 0 : parseFloat(amount);
            break;
      
    } // end switch

    // GET TOTAL EXPENSE
    this.totalExpense = (this.rent + this.maintenance + this.food + this.communication + this.utility 
                          + this.healthcare + this.childcare + this.carRental + this.other + this.tuition
                          + this.restaurant + this.travel + this.apparel + this.otherLeisure + this.creditCard
                          + this.otherBills + this.personalLoan);

  }

  submitForm(){

    // CHECK IF FORM IS VALID
    if(this.calcForm.valid) {
      document.getElementById("btnsubmit").setAttribute("disabled", "disabled");
      let formData = this.calcForm.value;
       // CALCULATE RESULT BASED ON DATA PASSED
       let calcResult = this.calculatorProvider.calculate(formData);
       // SHOW EXPENSE CALCULATOR RESULT PAGE
       this.navCtrl.push(ExpenseCalcResultPage,  {data : calcResult});
       document.getElementById("btnsubmit").removeAttribute("disabled");
    }
    else {
      // SHOW ERROR MESSAGE
      this.showToast('Invalid form. Please check your inputs.', 'middle');
      return false;
    }

  }

  showToast(message : string, position : string) {

      let toast = this.toastCtrl.create({
        message: message,
        duration: 3000,
        position: position
      });

      toast.present();
  }

}

