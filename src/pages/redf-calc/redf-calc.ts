import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Validators, FormBuilder, FormGroup } from "@angular/forms";
import { AppServiceApiProvider } from '../../providers/app-service-api/app-service-api';
import { CalculatorProvider }  from '../../providers/calculator/calculator';
import { RedfCalcResultPage } from '../redf-calc-result/redf-calc-result';
import { SpendingValidator } from  '../../validators/spending';
import { DownPaymentValidator } from  '../../validators/downpayment';
//import { GoogleAnalytics } from '@ionic-native/google-analytics';
import "rxjs/add/operator/map";
declare var dataLayer: Array<any>;

/**
 * Generated class for the RedfCalcPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-redf-calc',
  templateUrl: 'redf-calc.html',
})
export class RedfCalcPage {

  // DECLARE AND INITIALIZE PROPERTIES
  private calcForm : FormGroup; 

  public sectors = [];
  public sectorRates = [];

  public salaryFormatted = "";
  public salary = 0;

  public obligationFormatted = "";
  public obligation = 0;

  public propertyPriceFormatted = "";
  public propertyPrice = 0;

  public downPaymentFormatted = "";
  public downPayment = 0;

  public housingAllowanceFormatted = "";
  public housingAllowance = 0;

  public otherAllowanceFormatted = "";
  public otherAllowance = 0;

  public retirementFormatted = "";
  public retirement = 0;

  public dpIsHigher = false;

  objRes: any;
  

  constructor(private calculatorProvider : CalculatorProvider, 
    private http: HttpClient, 
    private formBuilder : FormBuilder, 
    public navCtrl: NavController, 
    public navParams: NavParams,
    private appServiceProvider : AppServiceApiProvider,
    private toastCtrl : ToastController) {

    // FORM VALIDATION
    this.calcForm = this.formBuilder.group({
      salary: ['', Validators.compose([Validators.required, Validators.min(4000)])],
      obligation: ['', Validators.required],
      propertyPrice: ['', Validators.required],
      downPayment: ['', Validators.required],
      housingAllowance: ['', Validators.required],
      otherAllowance: ['', Validators.required],
      retirement: ['', Validators.required],
      sector: ['', Validators.required],
      repayment_terms: ['', Validators.compose([Validators.required, Validators.min(3), Validators.max(30)])],
      interesttype: ['', Validators.required],
      dependents: ['', Validators.required],
      calcMode: ['5'],
      age: ['', Validators.compose([Validators.required, Validators.min(21), Validators.max(64)])],
      salaryFormatted: ['', Validators.required],
      obligationFormatted : ['', Validators.required],
      propertyPriceFormatted : ['', Validators.required],
      downPaymentFormatted : ['', Validators.required],
      housingAllowanceFormatted : ['', Validators.required],
      otherAllowanceFormatted : ['', Validators.required],
      retirementFormatted : ['', Validators.required],
      nationalId : ['', Validators.compose([Validators.required, Validators.minLength(10)])],
    }, { 'validator': [SpendingValidator.isValid,  DownPaymentValidator.isValid ]});

  }

  ionViewDidLoad() {
      console.log('ionViewDidLoad RedfCalcPage');

      dataLayer.push({
        'event' : 'appScreenView',
        'screenPath' : 'REDF Calc',
        'screenName' : 'REDF Calc',
      });

      // GET SECTORS
      this.appServiceProvider.getAppData()
          .subscribe(response => {
            this.sectors = response['select_sectors'];
        },
        error => {
          console.log(error);
      }); 


  }
  submitForm() {    
    // CHECK IF FORM IS VALID
    if(this.calcForm.valid) {
        document.getElementById("btnsubmit").setAttribute("disabled", "disabled");
        let formData = this.calcForm.value;
        // CALCULATE RESULT BASED ON DATA PASSED
        let calcResult = [];
      
        calcResult['sector'] = this.calcForm.controls.sector.value;
        calcResult = this.calculatorProvider.calculate(formData);

        if (!isNaN(calcResult['repayment_terms']) && calcResult['repayment_terms'] != 0) {
          
            let url = this.appServiceProvider.redfCalculateUrl;
            let headerOptions = { headers: {'Content-Type': 'application/x-www-form-urlencoded'} };
            let params = new HttpParams();
                params = params.append('lang', this.appServiceProvider.getCurrentLang());
                params = params.append('uname', this.appServiceProvider.getUsername());
                params =  params.append('secret', this.appServiceProvider.getSecret());
            
            // APPEND ADDITIONAL PARAMS
            Object.keys(calcResult).forEach( key => {    
                params =  params.append(key, calcResult[key]);
            });
            
            this.http.post( url, params, headerOptions )
            .subscribe((response) => {
                  this.objRes =  response;
                  if (this.objRes.status) {
                      calcResult['hasError']        = false;
                      calcResult['redf_name']       = this.objRes.data.name;
                      calcResult['fund_val']        = this.objRes.data.fund;
                      calcResult['emi_val']         = this.objRes.data.emi;
                      calcResult['eapr']            = this.objRes.data.eapr
                      calcResult['monthlyPayment']  = calcResult['emi'];
                      calcResult['maxAmount']       = calcResult['fund'];
                      calcResult['redf_mail_fund']  = this.objRes.data.fund;
                      calcResult['redf_mail_emi']   = this.objRes.data.emi;
                  } 
                  else   
                  {
                      calcResult['hasError'] = true;
                  }

                  //SHOW REDF CALCULATOR RESULT PAGE
                  this.navCtrl.push(RedfCalcResultPage,  {data : calcResult});
                  document.getElementById("btnsubmit").removeAttribute("disabled");
              });             
         }
         else
         {
            // SHOW ERROR MESSAGE
            this.showToast('Invalid form. Please check your inputs.', 'middle');
            return false;
         }
      
    }
    else {
      // SHOW ERROR MESSAGE
      this.showToast('Invalid form. Please check your inputs.', 'middle');
      return false;
    }
  }

  currencyFormat(event, key){

    let amount = event.target.value;
    amount = amount.replace(/[^0-9.]/g, "");
    let amountIsValid = isNaN(parseFloat(amount));

    switch(key) {

      case 'salaryFormatted':
            this.salaryFormatted = this.calculatorProvider.numberWithCommas(amount);
            this.salary = (amountIsValid)? 0 : parseFloat(amount);
            break;
   
      case 'obligationFormatted':
            this.obligationFormatted = this.calculatorProvider.numberWithCommas(amount);
            this.obligation = (amountIsValid)? 0 : parseFloat(amount);
            break;

      case 'propertyPriceFormatted':
            this.propertyPriceFormatted = this.calculatorProvider.numberWithCommas(amount);
            this.propertyPrice = (amountIsValid)? 0 : parseFloat(amount);
            break;
   
      case 'downPaymentFormatted':
            this.downPaymentFormatted = this.calculatorProvider.numberWithCommas(amount);
            this.downPayment = (amountIsValid)? 0 : parseFloat(amount);
            break;
      
      case 'housingAllowanceFormatted':
            this.housingAllowanceFormatted = this.calculatorProvider.numberWithCommas(amount);
            this.housingAllowance = (amountIsValid)? 0 : parseFloat(amount);
            break;
   
      case 'otherAllowanceFormatted':
            this.otherAllowanceFormatted = this.calculatorProvider.numberWithCommas(amount);
            this.otherAllowance = (amountIsValid)? 0 : parseFloat(amount);
            break;
          
      case 'retirementFormatted':
            this.retirementFormatted = this.calculatorProvider.numberWithCommas(amount);
            this.retirement = (amountIsValid)? 0 : parseFloat(amount);
            break;
    } // end switch

    if(this.downPayment > this.propertyPrice)
    {
      this.dpIsHigher = true;
    }
    else
    {
      this.dpIsHigher =  false;
    }

  }

  showToast(message : string, position : string) {

    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: position
    });

    toast.present();
  }

}
