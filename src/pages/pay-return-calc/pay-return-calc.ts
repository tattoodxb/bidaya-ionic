import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { HttpClient, HttpParams} from '@angular/common/http';
import { Validators, FormBuilder, FormGroup } from "@angular/forms";
import { AppServiceApiProvider } from '../../providers/app-service-api/app-service-api';
import { CalculatorProvider }  from '../../providers/calculator/calculator';
import { PayReturnCalcResultPage } from '../pay-return-calc-result/pay-return-calc-result';
//import { GoogleAnalytics } from '@ionic-native/google-analytics';
import 'rxjs/add/operator/map';
declare var dataLayer: Array<any>;
/**
 * Generated class for the PayReturnCalcPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-pay-return-calc',
  templateUrl: 'pay-return-calc.html',
})
export class PayReturnCalcPage {

  // DECLARE AND INITIALIZE PROPERTIES
  private calcForm : FormGroup;

  regions : any;
  sectors : any;
  public sectorRates = [];

  public monthlyInstallmentFormatted = "";
  public monthlyInstallment = 0;

  public companies = [];
  companyList : any;
  public company = "";


  constructor(private calculatorProvider : CalculatorProvider,
              private http: HttpClient, 
              private formBuilder : FormBuilder, 
              public navCtrl: NavController, 
              public navParams: NavParams,
              private appServiceProvider : AppServiceApiProvider,
              private toastCtrl : ToastController) {

    // FORM VALIDATION
    this.calcForm = this.formBuilder.group({
      monthlyInstallment: ['', Validators.required],
      age: ['', Validators.compose([Validators.required, Validators.min(21), Validators.max(64)])],
      location: ['', Validators.required],
      firsthomebuyer: ['', Validators.required],
      sector: ['', Validators.required],
      repayment_terms: ['', Validators.compose([Validators.required, Validators.min(3), Validators.max(30)])],
      interesttype: ['', Validators.required],
      calcMode: ['3'],
      company: [''],
      monthlyInstallmentFormatted: ['', Validators.required],
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PayReturnCalcPage');
    dataLayer.push({
      'event' : 'appScreenView',
      'screenPath' : 'Pay Return',
      'screenName' : 'Pay Return',
    });
    // GET REGIONS
    this.appServiceProvider.getAppData()
        .subscribe(response => {
          this.regions = response['regions'];
      },
      error => {
        console.log(error);
    });

     // GET SECTORS
    this.appServiceProvider.getAppData()
          .subscribe(response => {
            this.sectors = response['select_sectors'];
        },
        error => {
          console.log(error);
      }); 
  

  }

  submitForm() {

    // CHECK IF FORM IS VALID
    if(this.calcForm.valid) {
      document.getElementById("btnsubmit").setAttribute("disabled", "disabled");
      let formData = this.calcForm.value;
      let calcResult = [];
      calcResult['sector'] = this.calcForm.controls.sector.value; // ORIGINAL SECTOR

      if(this.calcForm.controls.company.value.trim() != '')
      { 

            let url = this.appServiceProvider.checkCompanyUrl;
            let headerOptions = { headers: {'Content-Type': 'application/x-www-form-urlencoded'} };
            let params = new HttpParams();
                params = params.append('lang', 'en');
                params = params.append('uname', this.appServiceProvider.getUsername());
                params = params.append('secret', this.appServiceProvider.getSecret());
                params = params.append('companyname', this.calcForm.controls.company.value);

            // SUBMIT FORM
            this.http.post( url, params, headerOptions )
                .subscribe((response) => {
                  // PASS NEW SECTOR RECEIVE FROM THE RESPONSE
                  if(response['value']!='')
                  {
                    formData['sector'] = response['value'];
                  }
                  // CALCULATE RESULT BASED ON THE INPUTS 
                  calcResult = this.calculatorProvider.calculate(formData);
                  // PUSH TO RESULT PAGE
                  this.navCtrl.push(PayReturnCalcResultPage,  {data : calcResult});
              },
              error => {
                console.log(error);
            });

      }
      else
      {
         // CALCULATE RESULT BASED ON THE INPUTS 
         calcResult = this.calculatorProvider.calculate(formData);  
         // PUSH TO RESULT PAGE
         this.navCtrl.push(PayReturnCalcResultPage,  {data : calcResult});
      }
      document.getElementById("btnsubmit").removeAttribute("disabled");
    }
    else {
      // SHOW ERROR MESSAGE
      this.showToast('Invalid form. Please check your inputs.', 'middle');
      return false;
    }
  }

  currencyFormat(event, key){

    let amount = event.target.value;

    switch(key) {

      case 'monthlyInstallmentFormatted':
            this.monthlyInstallment = amount.replace(/[^0-9.]/g, "");
            this.monthlyInstallmentFormatted = this.calculatorProvider.numberWithCommas(this.monthlyInstallment);
            break;
   
    } // end switch

  }

  autoCompleteCompany(event) {

    let keyword = event.target.value;
    // IF KEYWORD IS EMPTY CLEAR COMPANIES PROPERTY
    if(keyword.length == 0)
    {
       this.companies = [];
    }
    else
    {
      // CHECK IF COMPANY LIST IS EMPTY
      if(this.companyList == undefined) {
        
        // INITIALIZE COMPANY LIST
        let url = this.appServiceProvider.autofillCompanyUrl;
        let headerOptions = {headers: {'Content-Type': 'application/x-www-form-urlencoded'}};
        let params = new HttpParams();
            params = params.append('lang', this.appServiceProvider.getCurrentLang());
            params = params.append('uname', this.appServiceProvider.getUsername());
            params =  params.append('secret', this.appServiceProvider.getSecret());
          
        this.http.post( url, params, headerOptions )
          .map(response => response)
          .subscribe((response) => {
              this.companyList = response;
              this.companies = this.companyList.filter(item => item.toLowerCase().startsWith(keyword.toLowerCase()));
        }); 
      }
      else {
        // FILTER COMPANY LIST BASED ON KEYWORD
        this.companies = this.companyList.filter(item => item.toLowerCase().startsWith(keyword.toLowerCase()));
      }    
    }
  }

  itemSelect(item){
    this.company = item;
    // AFTER SELECT CLEAR COMPANIES DROPDOWN RESULT ARRAY
    this.companies = [];
  }

  showToast(message : string, position : string) {

    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: position
    });

    toast.present();
  }

}
