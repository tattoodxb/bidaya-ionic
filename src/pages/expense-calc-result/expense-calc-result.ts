import { Component } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { AppServiceApiProvider } from '../../providers/app-service-api/app-service-api';
declare var dataLayer: Array<any>;
/**
 * Generated class for the ExpenseCalcResultPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-expense-calc-result',
  templateUrl: 'expense-calc-result.html',
})
export class ExpenseCalcResultPage {

  public result = [];
  

  options : InAppBrowserOptions = {
    location : 'yes',//Or 'no' 
    hidden : 'no', //Or  'yes'
    clearcache : 'yes',
    clearsessioncache : 'yes',
    zoom : 'yes',//Android only ,shows browser zoom controls 
    hardwareback : 'yes',
    mediaPlaybackRequiresUserAction : 'no',
    shouldPauseOnSuspend : 'no', //Android only 
    closebuttoncaption : 'Close', //iOS only
    disallowoverscroll : 'no', //iOS only 
    toolbar : 'yes', //iOS only 
    enableViewportScale : 'no', //iOS only 
    allowInlineMediaPlayback : 'no',//iOS only 
    presentationstyle : 'pagesheet',//iOS only 
    fullscreen : 'yes',//Windows only    
  };

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private iab: InAppBrowser,
              private appServiceProvider : AppServiceApiProvider, private device: Platform) {

    this.result = navParams.get('data');
    console.log(this.result);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FinanceCalcResultPage');
    let platform = "Android";
    if (this.device.is('ios')) {
      platform = "Apple";
    }
    dataLayer.push({
      'event' : ' Calculator Usage',
      'category' : 'Calculator Usage',
      'action' : 'Expense Calculator',
      'label' : platform + ' app'
    });
  }

  openBrowser(url : string){
    let target = "_system";
    let host = "https://www.bidaya.com.sa/"+this.appServiceProvider.getCurrentLang()+"/";
    this.iab.create(host + url,target,this.options);
  }

}
