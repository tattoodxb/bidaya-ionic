import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppServiceApiProvider } from '../../providers/app-service-api/app-service-api';
import 'rxjs/add/operator/map';

/*
  Generated class for the CalculatorProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/


@Injectable()
export class CalculatorProvider {

  private calcMode       = 1;
  //private maxBarrow      = 0;
  private EMI            = 0;
  //private maxPerMonth    = 0;
  private sector         = "";
  private repaymentValue = 30;
  private flatRate       = 0;
  private eAPR           = 0;
  private defIntType     = "floating";
  private otherIntType     = "fixed";

  sectorRates : any;
  sectorBasedRates : any;

  private interestType;
  private newUser;
  private salary;
  private obligation;
  private repaymentTerm;
  private maxAmount;

  private propertyPrice;
  private downPayment;
  private repaymentTermMonth;
  private age;
  private location;
  private company;

  constructor(public http: HttpClient, private appServiceProvider : AppServiceApiProvider) {
    console.log('Hello CalculatorProvider Provider');
    
    this.appServiceProvider.getAppData()
       .subscribe(response => {
            this.sectorRates = response['sectors'];
        },
        error => {
          console.log(error);
      });

  }

  calculate(data) {

    var calcResult = [];
    calcResult["downPayment"]       = "";
    calcResult["propertyPrice"]     = "";
    calcResult["redf_mail_fund"]    = ""; 
    calcResult["redf_mail_emi"]     = ""; 
    calcResult["maxFinanceAmount"]  = "";
    calcResult["salaryemail"]       = "";
    calcResult["obligationemail"]   = "";
    calcResult["eapr"]              = "";
    calcResult["flatRate"]          = "";
    calcResult["repaymentTerm"]     = "";
    calcResult["emi"]               = "";
    calcResult["interesttype"]      = "";
    calcResult['ageemail']          = "";
    calcResult['location']          = "";
    calcResult['worklocation']      = "";
    calcResult['workhidden']        = "";
    calcResult['firstimehomebuyer'] = "";
    calcResult["calcType"]          = "";
    calcResult["repaymentyear"]     = "";

    if(Object.keys(data).length > 0)
    {

      this.calcMode = parseInt(data.calcMode);
      this.sector = data.sector;
    
      if(this.sector == 'SELFEMP') {
         this.repaymentValue = 25;
      }
      
      this.repaymentValue = this.getSectorMaxTerm(this.sector, data.interesttype);      
      var dsr;
      var leaseAmount;
      var leaseObligation;

      switch(this.calcMode) {
        case 1: // Finance Calc

            console.log('calculator 1 - Financial');
            this.age = parseInt(data.age);
            this.location = data.location;
            this.company  = data.company;

            this.interestType = data.interesttype;
            this.newUser = data.firsthomebuyer;
            this.salary = parseFloat(data.salary);
            this.obligation = parseFloat(data.obligation);
        
            this.repaymentTerm = parseInt(data.repayment_terms);    
            this.repaymentTerm = (this.repaymentTerm > this.repaymentValue) ? this.repaymentValue : this.repaymentTerm;
            calcResult["repaymentyear"] = this.repaymentTerm;
            
            //get the sector based rates
            this.sectorBasedRates = this.getSectorRates(this.sector, this.repaymentTerm, this.interestType);
            this.flatRate = this.sectorBasedRates.flatRate;
            this.eAPR = this.sectorBasedRates.eAPR;
            
            dsr  = this.getDSR(this.salary, this.sector);
            this.EMI = (this.salary * (dsr/100)) - this.obligation;
            this.EMI = (isNaN(this.EMI)) ? 0 : Math.round(this.EMI);

            this.repaymentTerm = this.repaymentTerm * 12;

            this.maxAmount = this.getMaxFinanceAmount(this.EMI, this.repaymentTerm, this.flatRate);

            calcResult["eapr"] = this.eAPR;
            calcResult["flatRate"] = this.flatRate;
            calcResult["repaymentTerm"] = this.repaymentTerm;
            calcResult["emi"] = this.numberWithCommas(this.EMI);
            calcResult["maxFinanceAmount"] = this.numberWithCommas(this.maxAmount);
            
            //for email
            calcResult["interesttype"] = this.interestType;
            calcResult["salaryemail"] = this.numberWithCommas(this.salary);
            calcResult["obligationemail"] = this.numberWithCommas(this.obligation);

            calcResult['ageemail'] = this.age;
            calcResult['location'] = this.location;
            calcResult['worklocation'] = this.company;
            calcResult['workhidden']  = this.sector;
            calcResult['firstimehomebuyer'] = this.newUser;
            calcResult["calcType"] = this.calcMode;
            
            return calcResult;
                
        case 2: // EMI Monthly Installment
            
            console.log('calculator 2 - Monthly Installment');
            this.age = parseInt(data.age);
            this.location = data.location;
            this.company  = data.company;

            this.interestType = data.interesttype;
            this.newUser = data.firsthomebuyer;
            this.propertyPrice = parseFloat(data.propertyPrice.replace(/\,/g,''));
            this.downPayment   = parseFloat(data.downPayment.replace(/\,/g,''));
            this.repaymentTerm = parseInt(data.repayment_terms);
                       
            this.repaymentTerm = (this.repaymentTerm > this.repaymentValue) ? this.repaymentValue : this.repaymentTerm;
            calcResult["repaymentyear"] = this.repaymentTerm;

            //get the sector based rates
            this.sectorBasedRates = this.getSectorRates(this.sector, this.repaymentTerm, this.interestType);
            this.flatRate = this.sectorBasedRates.flatRate;
            this.eAPR = this.sectorBasedRates.eAPR;

            this.repaymentTermMonth = this.repaymentTerm * 12; //convert to months
            leaseAmount = (this.propertyPrice - this.downPayment);
            leaseObligation = (leaseAmount * (this.flatRate/100) * this.repaymentTerm) + leaseAmount;
            this.EMI = leaseObligation / this.repaymentTermMonth;
            this.EMI = (isNaN(this.EMI)) ? 0 : Math.round(this.EMI);

            calcResult["eapr"] = this.eAPR;
            calcResult["flatRate"] = this.flatRate;
            calcResult["repaymentTerm"] = this.repaymentTermMonth;
            calcResult["emi"] = this.numberWithCommas(this.EMI);
            calcResult["downPayment"] = this.numberWithCommas(this.downPayment);
            calcResult["propertyPrice"] = this.numberWithCommas(this.propertyPrice);

            //for email
            calcResult["interesttype"] = this.interestType;
            calcResult['ageemail'] = this.age;
            calcResult['location'] = this.location;
            calcResult['worklocation'] = this.company;
            calcResult['workhidden']  = this.sector;
            calcResult['firstimehomebuyer'] = this.newUser;
            calcResult["calcType"] = this.calcMode;

            return calcResult;
        
        case 3: // Pay Return
            
            console.log('calculator 3 - Pay Return');

            this.age = parseInt(data.age);
            this.location = data.location;
            this.company  = data.company;

            this.EMI = parseFloat(data.monthlyInstallment);
            this.interestType = data.interesttype;
            this.newUser = data.firsthomebuyer;
            
            this.repaymentTerm = parseInt(data.repayment_terms);
            this.repaymentTerm = (this.repaymentTerm > this.repaymentValue) ? this.repaymentValue : this.repaymentTerm;
            calcResult["repaymentyear"] = this.repaymentTerm;

            //get the sector based rates
            this.sectorBasedRates = this.getSectorRates(this.sector, this.repaymentTerm, this.interestType);
            this.flatRate = this.sectorBasedRates.flatRate;
            this.eAPR = this.sectorBasedRates.eAPR;

            this.repaymentTermMonth = this.repaymentTerm * 12; //convert to months

            this.EMI = (isNaN(this.EMI)) ? 0 : Math.round(this.EMI);
                
            this.maxAmount =  this.getMaxFinanceAmount(this.EMI, this.repaymentTermMonth, this.flatRate);
                      
            calcResult["eapr"] = this.eAPR;
            calcResult["flatRate"] = this.flatRate;
            calcResult["repaymentTerm"] = this.repaymentTermMonth;
            calcResult["emi"] = this.numberWithCommas(this.EMI);
            calcResult["maxFinanceAmount"] = this.numberWithCommas(this.maxAmount);

            //for email
            calcResult["interesttype"] = this.interestType;
            calcResult['ageemail'] = this.age;
            calcResult['location'] = this.location;
            calcResult['worklocation'] = this.company;
            calcResult['workhidden']  = this.sector;
            calcResult['firstimehomebuyer'] = this.newUser;
            calcResult["calcType"] = this.calcMode;

            return calcResult;
        
        case 4: // Expense Calc
            let rem = parseFloat(data.salary) -  parseFloat(data.totalExpense);
            
            calcResult["emi"] = this.numberWithCommas(rem);
            calcResult["has_error"] = false;
            if(rem < 0)
            {
              calcResult["has_error"] = true;             
            }

            return calcResult;

        case 5:  //REDF
            console.log('calculator 5 - REDF');
            
            this.age = parseInt(data.age);
            this.interestType = data.interesttype;
            this.repaymentTerm = parseInt(data.repayment_terms);
            
            this.obligation = parseInt(data.obligation.replace(/\,/g,''));
            calcResult["obligation"] = this.obligation;

            this.repaymentTerm = (this.repaymentTerm >  this.repaymentValue) ?  this.repaymentValue : this.repaymentTerm;
            calcResult["repayment_terms"] =  this.repaymentTerm;
            calcResult["repaymentyear"]   = this.repaymentTerm;

            //get the sector based rates
            this.sectorBasedRates = this.getSectorRates(this.sector, this.repaymentTerm, this.interestType);

            this.flatRate = this.sectorBasedRates.flatRate;
            this.eAPR     = this.sectorBasedRates.eAPR;

            calcResult["flatRate"] = this.flatRate;
            
            let basic_salary = parseFloat(data.salary.replace(/\,/g,''));
            let housingAllowance = parseFloat(data.housingAllowance.replace(/\,/g,''));
            let retirement = parseFloat(data.retirement.replace(/\,/g,''));
            let otherAllowance = parseFloat(data.otherAllowance.replace(/\,/g,''));
            
            calcResult["basic_sal"] = basic_salary;
            calcResult["housing_sal"] = housingAllowance;
            calcResult["other_sal"] = otherAllowance;
            calcResult["retirement_sal"] = retirement;

            this.salary = (basic_salary + housingAllowance + otherAllowance) - (retirement);
            let repaymentTerm_var = parseInt(this.repaymentTerm);
            this.repaymentTermMonth = (repaymentTerm_var * 12);
            calcResult["repaymentTermMonth"] = this.repaymentTermMonth;
           
            // Salary based
            dsr = this.getDSR(this.salary, this.sector);
            this.EMI = (this.salary * (dsr/100)) - this.obligation;
            this.EMI = (isNaN(this.EMI)) ? 0 : Math.round(this.EMI);

            this.maxAmount =  this.getMaxFinanceAmount(this.EMI, this.repaymentTermMonth, this.flatRate);

             // property based
            this.propertyPrice = parseFloat(data.propertyPrice.replace(/\,/g,''));
            this.downPayment = parseFloat(data.downPayment.replace(/\,/g,''));
            leaseAmount = (this.propertyPrice - this.downPayment);
            leaseObligation = (leaseAmount * (this.flatRate/100) * repaymentTerm_var) + leaseAmount;
           
            let propEMI = leaseObligation / this.repaymentTermMonth;
            propEMI = (isNaN(propEMI)) ? 0 : Math.round(propEMI);

            let propMaxAmount = leaseAmount;

            if (propMaxAmount <= this.maxAmount) {

                 this.EMI = propEMI;
                 this.maxAmount = propMaxAmount
            }
            
            calcResult["fund"] = this.numberWithCommas(this.maxAmount);
            calcResult["emi"] = this.numberWithCommas(this.EMI);
            calcResult["age"] = this.age;
            calcResult["national_id"] = data.nationalId;
            calcResult["property_price"] = this.propertyPrice
            calcResult["down_payment"] =  this.downPayment;
            calcResult["interesttype"] = this.interestType;
            calcResult["no_of_depends"] = data.dependents;
            
            //for email
            calcResult['ageemail'] = this.age;
            calcResult['workhidden']  = this.sector;
            calcResult["calcType"] = this.calcMode;
            calcResult["downPayment"] = this.downPayment;
            calcResult["propertyPrice"] = this.propertyPrice;
            calcResult['obligationemail'] = this.numberWithCommas(this.obligation);
            calcResult['salaryemail'] = this.numberWithCommas(this.salary);
                   
            return calcResult;
        case 6:
            console.log('calculator 6 - Buy Rent');
            //var rate = 3.44;
            this.propertyPrice = parseInt(data.buyPropertyPrice);
            var downPayPerc    = parseInt(data.downPayment);
            this.repaymentTerm = parseInt(data.repayment_terms);
            this.sector = data.sector;

            this.repaymentValue = this.getSectorMaxTerm(this.sector, this.defIntType);
            this.repaymentTerm  = (this.repaymentTerm >  this.repaymentValue) ?  this.repaymentValue : this.repaymentTerm;

            var rent = parseFloat(data.rentPropertyPrice);
            this.sectorBasedRates = this.getSectorRates(this.sector, this.repaymentTerm, this.defIntType);
            this.flatRate = this.sectorBasedRates.flatRate;
            this.eAPR = this.sectorBasedRates.eAPR;

            this.downPayment = Math.round(this.propertyPrice * (downPayPerc/100));
            this.EMI = Math.round((((this.propertyPrice - this.downPayment) * (this.flatRate/100) * this.repaymentTerm) + (this.propertyPrice - this.downPayment) ) / (this.repaymentTerm * 12));
            var sellCost = this.propertyPrice + (this.propertyPrice * 50/100);
            var buyCost  = ((this.EMI * this.repaymentTerm * 12) + this.downPayment);
            var rentCost = (rent * 12 * this.repaymentTerm);

            calcResult["buyInitial"] = this.currencyFormat(this.downPayment);
            calcResult["rentInitial"] = this.currencyFormat(0);
            calcResult["buyEmi"] = this.currencyFormat(this.EMI);
            calcResult["rentEmi"] = this.currencyFormat(rent);
            calcResult["buyPaid"] = this.currencyFormat(this.EMI * 12);
            calcResult["rentPaid"] = this.currencyFormat(rent * 12);
            calcResult["buySell"] = this.currencyFormat(sellCost);
            calcResult["rentSell"] = this.currencyFormat(0);
            calcResult["buyCost"] = this.currencyFormat(buyCost);
            calcResult["rentCost"] = this.currencyFormat(rentCost);

            return calcResult;
             
       }//endswitch
    
      } //endif 
    
  }

  currencyFormat(val) {
    return this.numberWithCommas(val);
  }


  numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  getMaxFinanceAmount(EMI, repaymentTerm, flatRate){   
    flatRate = flatRate/100;
    let maxAmount = (((EMI * repaymentTerm) / (1+((flatRate/12) * repaymentTerm))));
    return (isNaN(maxAmount)) ? 0 : Math.round(maxAmount);
  }

  getDSR(income, sector) { 
    let dsr = 0;
    
    for (var i=0; i < this.sectorRates.length; i++) {
      if(sector == this.sectorRates[i].value.trim()) {
        let obj = JSON.parse(this.sectorRates[i].obligation);
        for (var a=0; a < obj.length; a++) {
          obj[a].max = (obj[a].max) ? obj[a].max : 10000000000000000;
          if(income >= obj[a].min && income <= obj[a].max) {
              dsr = obj[a].val;
          }
        }
      }
    }
    
    return dsr;
  }

  getSectorRates(sector, repaymentTerm, interestType){

    let rates = {
      flatRate : 0,
      eAPR : 0,
      type: this.defIntType
    };
    console.log(interestType);
    for (var i=0; i < this.sectorRates.length; i++) {

      if(sector == this.sectorRates[i].value.trim()) {
        let intObj = JSON.parse(this.sectorRates[i].interest);
        if(intObj.hasOwnProperty(interestType)) {    
          for (var a=0; a < intObj[interestType].length; a++) {
            if(repaymentTerm <= intObj[interestType][a].max ) {
              rates.flatRate = intObj[interestType][a].flat_rate;
              rates.eAPR = intObj[interestType][a].eapr;
              rates.type = interestType;
              return rates;
            }
          } // endfor child

        }
        else {
          if(interestType == this.defIntType)
            {
              rates = this.getSectorRates(sector, repaymentTerm, this.otherIntType);
            }
            else{
              rates = this.getSectorRates(sector, repaymentTerm, this.defIntType);
            }
          
        }
      }
   } // endfor parent

   return rates;

  }

  getSectorMaxTerm(sector, interestType) {

     //let rates = {};
     let max = 0;
     for (var i=0; i < this.sectorRates.length; i++) {
        if(sector == this.sectorRates[i].value.trim()) {
          let intObj = JSON.parse(this.sectorRates[i].interest);          
          if(intObj.hasOwnProperty(interestType)) {            
            for (var a=0; a < intObj[interestType].length; a++) {              
              if(intObj[interestType][a].max > max) {
                max = intObj[interestType][a].max;
              }
            } // endfor child
          }
          else {
            if(interestType == this.defIntType)
            {
              max = this.getSectorMaxTerm(sector, this.otherIntType);
            }
            else{
              max = this.getSectorMaxTerm(sector, this.defIntType);
            }            
          }
        }
     } // endfor parent

     return max;
  }

}
