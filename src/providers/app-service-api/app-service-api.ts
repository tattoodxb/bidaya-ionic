import { HttpClient , HttpParams} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import 'rxjs/add/operator/map';

/*
  Generated class for the AppServiceApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

@Injectable()
export class AppServiceApiProvider {

  private username          = "bidAppAdmin";
  private secret            = "bidApp@2018";
  public emailResultUrl     = "https://www.bidaya.com.sa/api/appservice/emailresult";
  public checkCompanyUrl    = "https://www.bidaya.com.sa/api/appservice/checkcompany";
  public autofillCompanyUrl = "https://www.bidaya.com.sa/api/appservice/autofillcompany";
  public redfCalculateUrl   = "https://www.bidaya.com.sa/api/appservice/redfcalculate";
  public appDataUrl         = "https://www.bidaya.com.sa/api/appservice/appdata";
  public gtmTag             = "GTM-TQLF6BW";

  constructor(public http: HttpClient,  public translate: TranslateService) {
    console.log('Hello AppServiceApiProvider Provider');
  }

  getAppData() {

    let url = this.appDataUrl;
    let headerOptions = { headers: {'Content-Type': 'application/x-www-form-urlencoded'} };
    let params = new HttpParams();
        params = params.append('lang', this.translate.currentLang);
        params = params.append('uname', this.username);
        params = params.append('secret', this.secret);
   
    return this.http.post( url, params, headerOptions );

  }

  getUsername() {
    return this.username;
  }

  getSecret() {
    return this.secret;
  }

  getCurrentLang() {
    return this.translate.currentLang;
  }

  generateFormData(result) {

      let calcresult = {
        "emailresult_name" : result['fullname'],
        "emailresult_email" : result['email'],
        "emailresult_number":result['number'],
        "salaryemail" : result['salaryemail'],
        "obligationemail" : result['obligationemail'],
        "repaymentyear" : result['repaymentyear'],
        "ageemail":  result['ageemail'],
        "maxAmount" : result['maxFinanceAmount'],
        "EMI" : result['emi'],
        "repaymentTerm" : result['repaymentTerm'],
        "flatRate" : result['flatRate'],
        "display_eapremail" : result['eapr'],
        "downPayment" : result['downPayment'],
        "propertyPrice" : result['propertyPrice'],
        "redf_mail_fund" : result['redf_mail_fund'],
        "redf_mail_emi": result['redf_mail_emi'],
        "calcType": result['calcType'],
      };

      let form = {
        "salary" : result['salaryemail'],
        "obligation" : result['obligationemail'],
        "location" : result['location'],
        "age":  result['ageemail'],
        "sector" : result['sector'],
        "repayment_terms" : result['repaymentyear'],
        "worklocation" : result['worklocation'],
        "workhidden" : result['workhidden'],
        "interesttype" : result['interesttype'],
      };

      let formdata = new HttpParams();
        Object.keys(form).forEach( key => {    
        formdata =  formdata.append(key, form[key]);
      });
      
      let postdata = {
        "result" : JSON.stringify(calcresult),
        "formdata" : formdata
      };

      return postdata;
  }

  getAlertTranslation() {

    let langData = [];

    if(this.getCurrentLang() == "ar")
    {
        langData['fullnameLabel']     =  'الاسم الكامل';
        langData['emailLabel']        =  'البريد الالكتروني';
        langData['mobileLabel']       =  'الهاتف المتحرك';
        langData['cancelButton']      =  "إلغاء";
        langData['sendButton']        =  "أرسل";
        langData['emailSentSuccess']  =  "تم ارسال البريد الإلكتروني";
        langData['emailSentError']    =  "غير قادر على إرسال البريد الإلكتروني. حاول مرة اخرى.";
        langData['pleaseWaitText']    =  "يرجى الانتظار...";
        langData['emailResultLabel']  =  "إرسال نتيجة البريد الإلكتروني";
    }
    else
    {
        langData['fullnameLabel']     =  "Fullname";
        langData['emailLabel']        =  "Email address";
        langData['mobileLabel']       =  "Mobile number";
        langData['cancelButton']      =  "Cancel";
        langData['sendButton']        =  "Submit";
        langData['emailSentSuccess']  =  "Email Sent";
        langData['emailSentError']    =  "Unable to send email, please try again.";
        langData['pleaseWaitText']    =  "Please wait...";
        langData['emailResultLabel']  =  "Email Result"
        langData['emailRequired']     =  "Email Result"
        langData['emailInvalid']      =  "Email Result"
    }

    return langData;

  }

}


