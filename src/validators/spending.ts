import { FormGroup  } from '@angular/forms';
 
export class SpendingValidator {
 
    static isValid(calcForm: FormGroup) {
       
        if(isNaN(calcForm.value['salary'])){
            return {
                "not a number": true
            };
        }
        
        if(isNaN(calcForm.value['obligation'])){
            return {
                "not a number": true
            };
        }
        
        if(((calcForm.value['obligation'] * 100) / calcForm.value['salary']) >= 60)
        {
            return {
                "spending should not exceed 60 percent of your earning": true
            };
        }
        return null;
    }
 
}