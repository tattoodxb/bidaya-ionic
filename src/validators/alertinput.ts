
 
export class AlertInputValidator {
 
    static emailIsValid(email : string) {

        var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;

        if(email.trim() == "")
        {
            console.log("email : This field is required");
            return {
                "Email field is required": true,
                "en" : "Email field is required",
                "ar" : "يرجى إدخال البريد الكتروني"
            };
        }

        if(!emailPattern.test(email))
        {
            console.log("email : Invalid email");
            return {
                "Invalid email": true,
                "en" : "Invalid email",
                "ar" : "يرجى ادخال بريد الكتروني صحيح"
            };
        }

        return null;
    }

    static fullnameIsValid(fullname : string) : any {
       
        if(fullname.trim() == "")
        {    
            console.log("name : Name field is required");
            return {
                "Name field is required": true,
                "en" : "Name field is required",
                "ar" : "يرجى إدخال الأسم كاملاً"
            };
        }
        return null;
    }

    static mobileIsValid(mobile : string) : any {
        var mobilePattern = /^\d*$/;
        
        if(mobile.trim() == "")
        {
            return {
                "Mobile field is required": true,
                "en" : "Mobile field is required",
                "ar" : "يرجى إدخال رقم الجوال"
            };
        }

        if(!mobilePattern.test(mobile))
        {
            return {
                "Invalid mobile number": true,
                "en" : "Invalid mobile number",
                "ar" : "الرجاء إدخال رقم جوال صحيح"
            };
        }

        if(mobile.length != 10 )
        {
            return {
                "Mobile number must contain 10": true,
                "en" : "Mobile number must contain 10",
                "ar" : "يجب ان يكون الهاتف 10 ارقام "
            };
        }

        if((mobile.slice(0, (2 - mobile.length))) != "05" )
        {
            return {
                "Mobile number should starts with 05": true,
                "en" : "Mobile number should starts with 05",
                "ar" : "يجب ان يكون الهاتف يبدأ بـ05"
            };
        }

        return null;
    }
 
}