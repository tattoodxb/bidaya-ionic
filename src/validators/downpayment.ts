import { FormGroup  } from '@angular/forms';
 
export class DownPaymentValidator {
 
    static isValid(calcForm: FormGroup) {
       
        if(isNaN(calcForm.value['downPayment'])){
            return {
                "not a number": true
            };
        }
        
        if(isNaN(calcForm.value['propertyPrice'])){
            return {
                "not a number": true
            };
        }

        if(parseFloat(calcForm.value['downPayment']) < (parseFloat(calcForm.value['propertyPrice']) / 15))
        {
            return {
                "down payment should not be less than 15% of property price": true
            };
        }
    
        
        if((parseFloat(calcForm.value['downPayment']) >= parseFloat(calcForm.value['propertyPrice'])))
        {
            //console.log("down payment has to be less than the property price");
            return {
                "down payment has to be less than the property price": true
            };
        }
        return null;
    }
 
}